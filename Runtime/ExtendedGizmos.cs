using UnityEngine;

namespace JonathanDefraiteur.Helpers
{
    public static class ExtendedGizmos
    {
        #region Color

        private static Color _retainedGizmosColor;
        public static Color Color
        {
            get => Gizmos.color;
            set => Gizmos.color = value;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_color"></param>
        /// <returns>Previous Gizmos.color</returns>
        public static Color SetColor(Color _color)
        {
            Color previousColor = Gizmos.color;
            Gizmos.color = _color;
            return previousColor;
        }
        
        public static void RetainColor()
        {
            _retainedGizmosColor = Gizmos.color;
        }

        public static void RetainColorAndChangeTo(Color _color)
        {
            ExtendedGizmos.RetainColor();
            Gizmos.color = _color;
        }

        public static void RestoreColorToRetained()
        {
            Gizmos.color = _retainedGizmosColor;
        }

        #endregion
        #region Matrix
        
        private static Matrix4x4 _retainedGizmosMatrix;
        public static Matrix4x4 Matrix
        {
            get => Gizmos.matrix;
            set => Gizmos.matrix = value;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_matrix"></param>
        /// <returns>Previous Gizmos.matrix</returns>
        public static Matrix4x4 SetMatrix(Matrix4x4 _matrix)
        {
            Matrix4x4 previousMatrix = Gizmos.matrix;
            Gizmos.matrix = _matrix;
            return previousMatrix;
        }
        
        public static void RetainMatrix()
        {
            _retainedGizmosMatrix = Gizmos.matrix;
        }

        public static void RetainMatrixAndChangeTo(Matrix4x4 _matrix)
        {
            ExtendedGizmos.RetainMatrix();
            Gizmos.matrix = _matrix;
        }

        public static void RestoreMatrixToRetained()
        {
            Gizmos.matrix = _retainedGizmosMatrix;
        }

        #endregion
        #region RectInt

        public static void DrawRect(RectInt _rect)
        {
            // Verticals
            Gizmos.DrawLine(new Vector3(_rect.xMin, _rect.yMin), new Vector3(_rect.xMin, _rect.yMax));
            Gizmos.DrawLine(new Vector3(_rect.xMax, _rect.yMin), new Vector3(_rect.xMax, _rect.yMax));
            // Horizontals
            Gizmos.DrawLine(new Vector3(_rect.xMin, _rect.yMin), new Vector3(_rect.xMax, _rect.yMin));
            Gizmos.DrawLine(new Vector3(_rect.xMin, _rect.yMax), new Vector3(_rect.xMax, _rect.yMax));
        }

        public static void DrawRect(RectInt _rect, bool _yToZ, float _thirdValue = 0f)
        {
            if (!_yToZ)
            {
                DrawRect(_rect);
                return;
            }
            
            // Verticals
            Gizmos.DrawLine(new Vector3(_rect.xMin, _thirdValue, _rect.yMin), new Vector3(_rect.xMin, _thirdValue, _rect.yMax));
            Gizmos.DrawLine(new Vector3(_rect.xMax, _thirdValue, _rect.yMin), new Vector3(_rect.xMax, _thirdValue, _rect.yMax));
            // Horizontals
            Gizmos.DrawLine(new Vector3(_rect.xMin, _thirdValue, _rect.yMin), new Vector3(_rect.xMax, _thirdValue, _rect.yMin));
            Gizmos.DrawLine(new Vector3(_rect.xMin, _thirdValue, _rect.yMax), new Vector3(_rect.xMax, _thirdValue, _rect.yMax));
        }

        public static void DrawRect(RectInt _rect, Color _color)
        {
            ExtendedGizmos.DrawRect(_rect, false, _color);
        }

        public static void DrawRect(RectInt _rect, bool _yToZ, Color _color)
        {
            ExtendedGizmos.DrawRect(_rect, _yToZ, 0f, _color);
        }

        public static void DrawRect(RectInt _rect, bool _yToZ, float _thirdValue, Color _color)
        {
            Color previousColor = ExtendedGizmos.SetColor(_color);
            ExtendedGizmos.DrawRect(_rect, _yToZ, _thirdValue);
            Gizmos.color = previousColor;
        }

        #endregion
        #region Rect

        public static void DrawRect(Rect _rect)
        {
            // Verticals
            Gizmos.DrawLine(new Vector3(_rect.xMin, _rect.yMin), new Vector3(_rect.xMin, _rect.yMax));
            Gizmos.DrawLine(new Vector3(_rect.xMax, _rect.yMin), new Vector3(_rect.xMax, _rect.yMax));
            // Horizontals
            Gizmos.DrawLine(new Vector3(_rect.xMin, _rect.yMin), new Vector3(_rect.xMax, _rect.yMin));
            Gizmos.DrawLine(new Vector3(_rect.xMin, _rect.yMax), new Vector3(_rect.xMax, _rect.yMax));
        }

        public static void DrawRect(Rect _rect, bool _yToZ, float _thirdValue = 0f)
        {
            if (!_yToZ)
            {
                DrawRect(_rect);
                return;
            }
            
            // Verticals
            Gizmos.DrawLine(new Vector3(_rect.xMin, _thirdValue, _rect.yMin), new Vector3(_rect.xMin, _thirdValue, _rect.yMax));
            Gizmos.DrawLine(new Vector3(_rect.xMax, _thirdValue, _rect.yMin), new Vector3(_rect.xMax, _thirdValue, _rect.yMax));
            // Horizontals
            Gizmos.DrawLine(new Vector3(_rect.xMin, _thirdValue, _rect.yMin), new Vector3(_rect.xMax, _thirdValue, _rect.yMin));
            Gizmos.DrawLine(new Vector3(_rect.xMin, _thirdValue, _rect.yMax), new Vector3(_rect.xMax, _thirdValue, _rect.yMax));
        }

        public static void DrawRect(Rect _rect, Color _color)
        {
            ExtendedGizmos.DrawRect(_rect, false, _color);
        }

        public static void DrawRect(Rect _rect, bool _yToZ, Color _color)
        {
            ExtendedGizmos.DrawRect(_rect, _yToZ, 0f, _color);
        }

        public static void DrawRect(Rect _rect, bool _yToZ, float _thirdValue, Color _color)
        {
            Color previousColor = ExtendedGizmos.SetColor(_color);
            ExtendedGizmos.DrawRect(_rect, _yToZ, _thirdValue);
            Gizmos.color = previousColor;
        }

        #endregion
        #region DrawCube
        
        private static void DrawGeneralCube(Vector3 _center, Vector3 _size, Color _color, bool _wire)
        {
            Color previousColor = ExtendedGizmos.SetColor(_color);
            if (_wire) {
                Gizmos.DrawWireCube(_center, _size);
            }
            else {
                Gizmos.DrawCube(_center, _size);
            }
            Gizmos.color = previousColor;
        }

        private static void DrawGeneralCube(Vector3 _center, Vector3 _size, Quaternion _rotation, Color _color, bool _wire)
        {
            var newMatrix = Matrix4x4.TRS(_center, _rotation, _size);
            Matrix4x4 previousMatrix = ExtendedGizmos.SetMatrix(newMatrix);
            ExtendedGizmos.DrawGeneralCube(Vector3.zero, Vector3.one, _color, _wire);
            Gizmos.matrix = previousMatrix;
        }
        
        #endregion
        #region Cube

        public static void DrawCube(Vector3 _center, Vector3 _size)
            => Gizmos.DrawCube(_center, _size);

        public static void DrawCube(Vector3 _center, Vector3 _size, Color _color)
            => ExtendedGizmos.DrawGeneralCube(_center, _size, _color, false);

        public static void DrawCube(Vector3 _center, Vector3 _size, Quaternion _rotation)
            => ExtendedGizmos.DrawGeneralCube(_center, _size, _rotation, Gizmos.color, false);

        public static void DrawCube(Vector3 _center, Vector3 _size, Quaternion _rotation, Color _color)
            => ExtendedGizmos.DrawGeneralCube(_center, _size, _rotation, _color, false);

        public static void DrawCube(Bounds _bounds)
            => Gizmos.DrawCube(_bounds.center, _bounds.size);

        public static void DrawCube(Bounds _bounds, Color _color)
            => ExtendedGizmos.DrawCube(_bounds.center, _bounds.size, _color);

        public static void DrawCube(Bounds _bounds, Quaternion _rotation)
            => ExtendedGizmos.DrawCube(_bounds.center, _bounds.size, _rotation);

        public static void DrawCube(Bounds _bounds, Quaternion _rotation, Color _color)
            => ExtendedGizmos.DrawCube(_bounds.center, _bounds.size, _rotation, _color);

        public static void DrawCube(BoundsInt _bounds)
            => Gizmos.DrawCube(_bounds.center, _bounds.size);

        public static void DrawCube(BoundsInt _bounds, Color _color)
            => ExtendedGizmos.DrawCube(_bounds.center, _bounds.size, _color);

        public static void DrawCube(BoundsInt _bounds, Quaternion _rotation)
            => ExtendedGizmos.DrawCube(_bounds.center, _bounds.size, _rotation);

        public static void DrawCube(BoundsInt _bounds, Quaternion _rotation, Color _color)
            => ExtendedGizmos.DrawCube(_bounds.center, _bounds.size, _rotation, _color);

        #endregion
        #region WireCube

        public static void DrawWireCube(Vector3 _center, Vector3 _size)
            => Gizmos.DrawWireCube(_center, _size);

        public static void DrawWireCube(Vector3 _center, Vector3 _size, Color _color)
            => ExtendedGizmos.DrawGeneralCube(_center, _size, _color, true);

        public static void DrawWireCube(Vector3 _center, Vector3 _size, Quaternion _rotation)
            => ExtendedGizmos.DrawGeneralCube(_center, _size, _rotation, Gizmos.color, true);

        public static void DrawWireCube(Vector3 _center, Vector3 _size, Quaternion _rotation, Color _color)
            => ExtendedGizmos.DrawGeneralCube(_center, _size, _rotation, _color, true);

        public static void DrawWireCube(Bounds _bounds)
            => Gizmos.DrawWireCube(_bounds.center, _bounds.size);

        public static void DrawWireCube(Bounds _bounds, Color _color)
            => ExtendedGizmos.DrawWireCube(_bounds.center, _bounds.size, _color);

        public static void DrawWireCube(Bounds _bounds, Quaternion _rotation)
            => ExtendedGizmos.DrawWireCube(_bounds.center, _bounds.size, _rotation);

        public static void DrawWireCube(Bounds _bounds, Quaternion _rotation, Color _color)
            => ExtendedGizmos.DrawWireCube(_bounds.center, _bounds.size, _rotation, _color);

        public static void DrawWireCube(BoundsInt _bounds)
            => Gizmos.DrawWireCube(_bounds.center, _bounds.size);

        public static void DrawWireCube(BoundsInt _bounds, Color _color)
            => ExtendedGizmos.DrawWireCube(_bounds.center, _bounds.size, _color);

        public static void DrawWireCube(BoundsInt _bounds, Quaternion _rotation)
            => ExtendedGizmos.DrawWireCube(_bounds.center, _bounds.size, _rotation);

        public static void DrawWireCube(BoundsInt _bounds, Quaternion _rotation, Color _color)
            => ExtendedGizmos.DrawWireCube(_bounds.center, _bounds.size, _rotation, _color);

        #endregion
        #region DrawSphere

        //private static void DrawGeneralSphere(Vector3 _center, float _radius, Quaternion _rotation, Color _color, bool _wire)
        //    => DrawGeneralSphere(_center, Vector3.one / 2 * _radius, _rotation, _color, _wire);

        public static void DrawGeneralSphere(Vector3 _center, float _radius, Color _color, bool _wire)
        {
            Color previousColor = ExtendedGizmos.SetColor(_color);
            if (_wire)
                Gizmos.DrawWireSphere(_center, _radius);
            else
                Gizmos.DrawSphere(_center, _radius);
            Gizmos.color = previousColor;
        }

        public static void DrawGeneralSphere(Vector3 _center, float _radius, Quaternion _rotation, Color _color, bool _wire)
            => DrawGeneralSphere(_center, Vector3.one / 2 * _radius, _rotation, _color, _wire);

        public static void DrawGeneralSphere(Vector3 _center, Vector3 _size, Quaternion _rotation, Color _color, bool _wire)
        {
            var newMatrix = Matrix4x4.TRS(_center, _rotation, _size);
            Matrix4x4 previousMatrix = ExtendedGizmos.SetMatrix(newMatrix);
            ExtendedGizmos.DrawGeneralSphere(Vector3.zero, .5f, _color, _wire);
            Gizmos.matrix = previousMatrix;
        }

        #endregion
        #region Sphere

        public static void DrawSphere(Vector3 _center, float _radius)
            => Gizmos.DrawSphere(_center, _radius);

        public static void DrawSphere(Vector3 _center, float _radius, Color _color)
            => ExtendedGizmos.DrawGeneralSphere(_center, _radius, Quaternion.identity, _color, false);

        public static void DrawSphere(Vector3 _center, float _radius, Quaternion _rotation)
            => ExtendedGizmos.DrawGeneralSphere(_center, _radius, _rotation, Gizmos.color, false);

        public static void DrawSphere(Vector3 _center, float _radius, Quaternion _rotation, Color _color)
            => ExtendedGizmos.DrawGeneralSphere(_center, _radius, _rotation, _color, false);

        public static void DrawSphere(Vector3 _center, Vector3 _size)
            => ExtendedGizmos.DrawGeneralSphere(_center, _size, Quaternion.identity, Gizmos.color, false);

        public static void DrawSphere(Vector3 _center, Vector3 _size, Color _color)
            => ExtendedGizmos.DrawGeneralSphere(_center, _size, Quaternion.identity, _color, false);

        public static void DrawSphere(Vector3 _center, Vector3 _size, Quaternion _rotation)
            => ExtendedGizmos.DrawGeneralSphere(_center, _size, _rotation, Gizmos.color, false);

        public static void DrawSphere(Vector3 _center, Vector3 _size, Quaternion _rotation, Color _color)
            => ExtendedGizmos.DrawGeneralSphere(_center, _size, _rotation, _color, false);

        public static void DrawSphere(Bounds _bounds)
            => ExtendedGizmos.DrawSphere(_bounds.center, _bounds.size);

        public static void DrawSphere(Bounds _bounds, Color _color)
            => ExtendedGizmos.DrawSphere(_bounds.center, _bounds.size, _color);

        public static void DrawSphere(Bounds _bounds, Quaternion _rotation)
            => ExtendedGizmos.DrawSphere(_bounds.center, _bounds.size, _rotation);

        public static void DrawSphere(Bounds _bounds, Quaternion _rotation, Color _color)
            => ExtendedGizmos.DrawSphere(_bounds.center, _bounds.size, _rotation, _color);

        public static void DrawSphere(BoundsInt _bounds)
            => ExtendedGizmos.DrawSphere(_bounds.center, _bounds.size);

        public static void DrawSphere(BoundsInt _bounds, Color _color)
            => ExtendedGizmos.DrawSphere(_bounds.center, _bounds.size, _color);

        public static void DrawSphere(BoundsInt _bounds, Quaternion _rotation)
            => ExtendedGizmos.DrawSphere(_bounds.center, _bounds.size, _rotation);

        public static void DrawSphere(BoundsInt _bounds, Quaternion _rotation, Color _color)
            => ExtendedGizmos.DrawSphere(_bounds.center, _bounds.size, _rotation, _color);

        #endregion
        #region WireSphere

        public static void DrawWireSphere(Vector3 _center, float _radius)
            => Gizmos.DrawWireSphere(_center, _radius);

        public static void DrawWireSphere(Vector3 _center, float _radius, Color _color)
            => ExtendedGizmos.DrawGeneralSphere(_center, _radius, Quaternion.identity, _color, true);

        public static void DrawWireSphere(Vector3 _center, float _radius, Quaternion _rotation)
            => ExtendedGizmos.DrawGeneralSphere(_center, _radius, Quaternion.identity, Gizmos.color, true);

        public static void DrawWireSphere(Vector3 _center, float _radius, Quaternion _rotation, Color _color)
            => ExtendedGizmos.DrawGeneralSphere(_center, _radius, _rotation, _color, true);

        public static void DrawWireSphere(Vector3 _center, Vector3 _size)
            => ExtendedGizmos.DrawGeneralSphere(_center, _size, Quaternion.identity, Gizmos.color, true);

        public static void DrawWireSphere(Vector3 _center, Vector3 _size, Color _color)
            => ExtendedGizmos.DrawGeneralSphere(_center, _size, Quaternion.identity, _color, true);

        public static void DrawWireSphere(Vector3 _center, Vector3 _size, Quaternion _rotation)
            => ExtendedGizmos.DrawGeneralSphere(_center, _size, _rotation, Gizmos.color, true);

        public static void DrawWireSphere(Vector3 _center, Vector3 _size, Quaternion _rotation, Color _color)
            => ExtendedGizmos.DrawGeneralSphere(_center, _size, _rotation, _color, true);

        public static void DrawWireSphere(Bounds _bounds)
            => ExtendedGizmos.DrawWireSphere(_bounds.center, _bounds.size);

        public static void DrawWireSphere(Bounds _bounds, Color _color)
            => ExtendedGizmos.DrawWireSphere(_bounds.center, _bounds.size, _color);

        public static void DrawWireSphere(Bounds _bounds, Quaternion _rotation)
            => ExtendedGizmos.DrawWireSphere(_bounds.center, _bounds.size, _rotation);

        public static void DrawWireSphere(Bounds _bounds, Quaternion _rotation, Color _color)
            => ExtendedGizmos.DrawWireSphere(_bounds.center, _bounds.size, _rotation, _color);

        public static void DrawWireSphere(BoundsInt _bounds)
            => ExtendedGizmos.DrawWireSphere(_bounds.center, _bounds.size);

        public static void DrawWireSphere(BoundsInt _bounds, Color _color)
            => ExtendedGizmos.DrawWireSphere(_bounds.center, _bounds.size, _color);

        public static void DrawWireSphere(BoundsInt _bounds, Quaternion _rotation)
            => ExtendedGizmos.DrawWireSphere(_bounds.center, _bounds.size, _rotation);

        public static void DrawWireSphere(BoundsInt _bounds, Quaternion _rotation, Color _color)
            => ExtendedGizmos.DrawWireSphere(_bounds.center, _bounds.size, _rotation, _color);

        #endregion
    }
}